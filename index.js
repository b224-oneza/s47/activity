// alert("Hello Batch 224!");

// querySelector() is a method that can be used to select a specific element from our document. 
console.log(document.querySelector("#txt-first-name"));

// document refers to the whole page
console.log(document);

/* 
    Alternative ways to access HTML elements. This is what we can use aside from the querySelector().

        document.getElementById("txt-first-name");
        document.getElementByClassName("txt-first-name")
        document.getElementByTagName("input")

*/

// Example:
// getElementById() - is similar to the querySelector()
 console.log(document.getElementById("txt-first-name"));


 // getElementByClassName() - methods returns a collection of elements with a specified class names/s.
 // It returns an HTML Collection and property is read-only
 console.log(document.getElementsByClassName("txt-inputs"));

 // getElementByTagName() - methods returns a collection of all elements with a specified tag name/s.
 // It returns an HTMLCollection and property 
 console.log(document.getElementsByTagName("input"));


 
 // Event and Event Listeners 

 const  txtFirtsName = document.querySelector("#txt-first-name");
 const  spanFullName = document.querySelector("#span-full-name");
console.log(txtFirtsName);
console.log(spanFullName);


/*
    Event
        ex: click, hover, keypress and many other events.

    Event Listeners
        Allows us to let out user/s interact with our group. With each clock or hover there is a event which triggers a function/task

    Syntax:
        selectedElement.addEventListener("event", function)

*/


// txtFirtsName.addEventListener("keyup", (event) => {
    
//     spanFullName.innerHTML = txtFirtsName.value
// });


/*
    .innerHTML - is a property of an  which is considers all the childeren of the selected element as a string

    .value - this property contains the value of an element.

*/

// Another way to write the code for event handling
txtFirtsName.addEventListener("keyup", printFirstName);

function printFirstName (event) {
    spanFullName.innerHTML = txtFirtsName.value

};

txtFirtsName.addEventListener("keyup", (event) => {
    console.log(event);
    console.log(event.target);
    console.log(event.target.value);
});


/*
	The "event" argument contains the information on the triggered event.
	The "event.target" contains the element where the event happened.
	the "event.target.value" gets the value of the input object (this is similar to txtFirstName.value).
*/

const labelFirstName = document.querySelector("#label-first-name");
console.log(labelFirstName);

labelFirstName.addEventListener("click", (e) => {
    console.log(e);
    console.log(e.target);
    alert("You clicked the First Name label.")
})

// ACTIVITY S47
// Last Name input change
txtLastName.addEventListener("keyup", printLastName);

function printLastName (e) {
    spanFullName.innerHTML = `${txtFirtsName.value} ${txtLastName.value}`
};

txtLastName.addEventListener("keyup", (e) => {
    console.log(e);
    console.log(e.target);
    console.log(e.target.value)
})